#!/bin/bash

clear

echo "Levantando servicio Pm2"
service pm2 start
pm2 list
echo

echo "Levantando servicio Apache2"
service apache2 start
service apache2 status
echo

echo "Levantando servicio mysql"
service mysql start
service mysql status
echo

